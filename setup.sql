--necessary database (oracle) tables for the skeleton

--see JdbcMessageSource.java for interaction with this table for i18n support
CREATE TABLE ref_resource_bundle
(
  text varchar2(4000) NOT NULL,
  text_id varchar2(60) NOT NULL,
  language varchar2(2) NOT NULL,
  CONSTRAINT ref_resource_bundle_pkey PRIMARY KEY (text_id , language )
);

--these tables are used to map roles (from ldap) to capablities when the user logs in to be stored in their granted authorizations
--spring security will automatically prefix our roles with ROLE_ (by default, this can be changed in the xml config) so make sure thats included here
CREATE TABLE auth_role
(
	role varchar2(50) NOT NULL PRIMARY KEY,
	description varchar2(200) NOT NULL
);

CREATE TABLE auth_cap
(
	cap varchar2(50) NOT NULL PRIMARY KEY,
	description varchar2(200) NOT NULL
);

CREATE TABLE auth_role_cap_map
(
  role varchar2(50) NOT NULL,
  cap varchar2(50) NOT NULL,
  CONSTRAINT role_cap_map_pkey PRIMARY KEY (role , cap),
  CONSTRAINT role_fk FOREIGN KEY (role) REFERENCES auth_role (role),
  CONSTRAINT cap_fk FOREIGN KEY (cap) REFERENCES auth_cap (cap)
);


--example
--insert into auth_role values('ROLE_SYSADMIN', 'Can access everything')
--insert into auth_cap values('CAP_READ_ALL', 'Can read everything')
--insert into auth_cap values('CAP_WRITE_ALL', 'Can update everything')
--insert into auth_role_cap_map values('ROLE_SYSADMIN', 'CAP_READ_ALL')
--insert into auth_role_cap_map values('ROLE_SYSADMIN', 'CAP_WRITE_ALL')
--you would then reference  CAP_READ_ALL and CAP_WRITE_ALL in your code.

--table to load application properties from
--these will be accessable via ${property.key} in spring xml files or @Value("#{property.key}") annotation on fields
--see DatabaseConfig.java properties bean for where it is loaded.
CREATE TABLE app_config 
(
	key varchar2(50) primary key,
	value varchar2(200) not null
);

--this is your ldap server and login information.  this needs to be configured in each environment.
--these are used in the application-context-spring-security.xml spring configuration
--these are not used in the dev profile since it uses an embedded server.

insert into app_config values('ldap.server.url', 'ldap://localhost:10389/o=myorg');
insert into app_config values('ldap.manager.dn', 'managerDN');
insert into app_config values('ldap.manager.password', 'secret');

--these are the default values for our embedded ldap server.  these values should be updated for each non dev environment
--these are used in the application-context-spring-security-common.xml spring configuration
--these are the default values for the embedded ldap server for the dev profile, you need to update the users.ldif file if you are going change these for the dev profile.
insert into app_config values('ldap.user.search.base', 'ou=users');
insert into app_config values('ldap.user.search.filter', '(uid={0})');
insert into app_config values('ldap.group.search.base', 'ou=groups');
insert into app_config values('ldap.group.search.filter', '(uniqueMember={0})');
insert into app_config values('ldap.group.role.attribute', 'cn');


--this table is only necessary if you are using Remember Me authencation functionality
--CREATE TABLE persistent_logins
--(
--  username varchar2(100) NOT NULL,
--  series varchar2(100) NOT NULL,
--  token varchar2(100) NOT NULL,
--  last_used date,
--  CONSTRAINT persistent_logins_pkey PRIMARY KEY (series )
--)