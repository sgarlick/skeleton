package com.company.artifact.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Primary configuration for service layer beans.
 * 
 * @author stephen.garlick
 * 
 */
@Configuration
@ComponentScan("com.company.artifact.service")
public class ServiceConfig {

}
