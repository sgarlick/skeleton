package com.company.artifact.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Our initial page after logging in.
 * 
 * @author stephen.garlick
 * 
 */
@Controller
@RequestMapping(value = "/")
public class HomePageController {
	@RequestMapping(method = RequestMethod.GET)
	public String view(ModelMap modelMap) {
		return "home";
	}

}
