package com.company.artifact.web.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.company.artifact.web.controller.InternalServerErrorController;

public class InternalServerErrorControllerTest {
	private InternalServerErrorController controller;

	@Before
	public void setup() {
		controller = new InternalServerErrorController();
	}

	@Test
	public void view() {
		String view = controller.view(null);
		assertThat(
				"InternalServerErrorController should return error/500 view",
				view, is("error/500"));
	}
}
