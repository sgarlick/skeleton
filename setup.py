import os
import sys
import argparse
import inspect


def _textReplace(package, path, artifact, name):
	#values to replace in the project files
	packageToReplace = "com.company.artifact"
	pathToReplace = "com/company/artifact"
	applicationNameToRepalce = "ProgramNameReplace"
	artifactToReplace = "artifactrep"

	for root, dirs, files in os.walk(".", topdown=False):
		if ".git" in root:
			continue
		if ".gitignore" in files:
			files.remove(".gitignore")
		if "setup.py" in files:
			files.remove("setup.py")
		if "temp.py" in files:
			files.remove("temp.py")
		for file in files:
			print ('replacing text in '+os.path.join(root, file))
			text_file = open(os.path.join(root,file), 'r')
			text = text_file.read()
			text_file.close()
			
			text = text.replace(packageToReplace, package)
			text = text.replace(pathToReplace, path)
			text = text.replace(artifactToReplace, artifact)
			text = text.replace(applicationNameToRepalce, name)
			
			text_file = open(os.path.join(root,file), 'w')
			text_file.write(text)
			text_file.close()

#
			
def _folderRename(path, artifact):
	print("renaming folders")
	os.renames("artifactrep-core", artifact+"-core");
	os.renames("artifactrep-webapp", artifact+"-webapp");
	os.renames("artifactrep-service", artifact+"-service");
	os.renames("artifactrep-ear", artifact+"-ear");

	os.renames(artifact+"-core\\src\\main\\java\\com\\company\\artifact", artifact+"-core\\src\\main\\java\\"+path)
	os.renames(artifact+"-core\\src\\test\\java\\com\\company\\artifact", artifact+"-core\\src\\test\\java\\"+path)
	
	os.renames(artifact+"-webapp\\src\\main\\java\\com\\company\\artifact", artifact+"-webapp\\src\\main\\java\\"+path)
	os.renames(artifact+"-webapp\\src\\main\\resources\\com\\company\\artifact", artifact+"-webapp\\src\\main\\resources\\"+path)
	os.renames(artifact+"-webapp\\src\\test\\java\\com\\company\\artifact", artifact+"-webapp\\src\\test\\java\\"+path)
	
	os.renames(artifact+"-service\\src\\main\\java\\com\\company\\artifact", artifact+"-service\\src\\main\\java\\"+path)
#

def _main(args):
	argparser = argparse.ArgumentParser(description=inspect.getdoc(_main))
	argparser.add_argument('-a', '--artifact', dest = "artifact", metavar = "name",  help = "The name of the project artifact, converted to lowercase")
	argparser.add_argument('-d', '--domain', dest = 'domain', default ="com.usps", help = "The base package domain to be combined with the artifact name, converted to lowercase.  Default is com.usps")
	argparser.add_argument('-n', '--name', dest = 'name', metavar = 'name',  help = "The project display name.  Use quotes")
	
	opts = argparser.parse_args(args)
	opts.domain = opts.domain.lower()
	opts.artifact = opts.artifact.lower()
	_folderRename(opts.domain.replace('.', '\\')+'\\'+opts.artifact, opts.artifact)
	_textReplace(opts.domain+'.'+opts.artifact, opts.domain.replace('.', '/')+'/'+opts.artifact, opts.artifact, opts.name)
#
if __name__ == "__main__":
	_main(sys.argv[1:])
	