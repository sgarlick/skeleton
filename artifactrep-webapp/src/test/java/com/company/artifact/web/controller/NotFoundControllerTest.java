package com.company.artifact.web.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.company.artifact.web.controller.NotFoundController;

public class NotFoundControllerTest {
	private NotFoundController controller;

	@Before
	public void setup() {
		controller = new NotFoundController();
	}

	@Test
	public void view() {
		String view = controller.view(null);
		assertThat("NotFoundController should return error/404 view", view,
				is("error/404"));
	}
}
