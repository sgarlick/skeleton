package com.company.artifact.core.security.permissions;

import org.springframework.security.core.Authentication;

import com.company.artifact.core.config.SecurityConfig;

/**
 * Implementations of this interface are intended to provide custom pragmatic
 * security constraints (ie a database check to see who created a record)
 * 
 * See {@link DefaultPermissionEvaluatorImpl} bean in {@link SecurityConfig}
 * 
 * @author stephen.garlick
 * 
 */
public interface Permission {

	public boolean isAllowed(Authentication authentication, Object targetObject);
}
