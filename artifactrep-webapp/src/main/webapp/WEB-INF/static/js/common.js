$(document).ready(function () {
	//this is a simple function that allows us to serialize forms to json using the name / value attributes
	$.fn.serializeObject = function() {
		var o = {};
		$.each(this.serializeArray(), function() {
			if (o[this.name] !== undefined) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
	
	$.ajaxSetup({
		cache : false,
		error : function(data) {
			//our app returns 403 when an ajax call is made against a invalid session
			//we then reload the page and let it redirect to the login page.
			if(data.status == 403)
				location.reload();
		}
	});
});